package com.progressoft.chatapp.model

enum class Gender {
    FEMALE,
    MALE
}
