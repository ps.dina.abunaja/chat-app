package com.progressoft.chatapp.model

enum class MessageStatus {
    JOIN,
    MESSAGE,
    LEAVE
}