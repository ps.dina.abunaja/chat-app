package com.progressoft.chatapp.model

enum class Role {
    USER,
    ADMIN
}