package com.progressoft.chatapp.model

import com.progressoft.chatapp.controller.entity.UserResponse
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class User(
    @Indexed(unique = true) val username: String,
    val firstName: String,
    val lastName: String,
    val gender: Gender,
    @Indexed(unique = true) val email: String,
    val password: String,
    var friends: MutableList<UserResponse> = mutableListOf(),
    val roles: Role = Role.USER
) {
    @Id
    lateinit var id: String
}
