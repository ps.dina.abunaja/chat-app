package com.progressoft.chatapp.model

import java.time.LocalDateTime
import lombok.Data
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Data
@Document
data class ChatMessage(
    @Id
    val messageId: String,
    val message: String?,
    val senderName: String?,
    val receiverName: String?,
    val status: MessageStatus?,
    val dateTime: LocalDateTime = LocalDateTime.now()
)
