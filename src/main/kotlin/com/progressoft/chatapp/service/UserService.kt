package com.progressoft.chatapp.service

import com.progressoft.chatapp.exceptions.DuplicatedEntityException
import com.progressoft.chatapp.exceptions.NoEntityFoundException
import com.progressoft.chatapp.model.User
import com.progressoft.chatapp.controller.entity.UserResponse
import com.progressoft.chatapp.controller.entity.toResponse
import com.progressoft.chatapp.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService {
    @Autowired
    lateinit var userRepository: UserRepository

    fun listAllUsers(): List<User> = userRepository.findAll()

    fun addNewUser(user: User): User {
        return if (!userRepository.existsByEmail(user.email) && !userRepository.existsByUsername(user.username)) {
            log.info("add new user with name ${user.username}")
            userRepository.insert(user)
        } else {
            throw DuplicatedEntityException("user with email ${user.email} is already exist or username ${user.username} is token")
        }
    }

    fun deleteUserByEmail(email: String) {
        if (!userRepository.existsByEmail(email)) {
            throw NoEntityFoundException("User with email $email not exist")
        }
        log.info("delete user with email $email")
        userRepository.delete(userRepository.getUserByEmail(email))
    }

    fun getUserByEmail(email: String): User {
        if (userRepository.existsByEmail(email))
            return userRepository.getUserByEmail(email)
        else {
            throw NoEntityFoundException("User with email $email not exist")
        }
    }

    fun getUserByUsername(username: String): User {
        if (userRepository.existsByUsername(username)) {
            log.info("return user with username if exists $username")
            return userRepository.getUserByUsername(username)
        } else {
            throw NoEntityFoundException("User with username $username not exist")
        }
    }

    fun updateUser(user: User, id: String): User {
        return withUser(id) {
            user.id = id
            return@withUser userRepository.save(user)
        }
    }

    fun addFriend(username: String, usernameOfFriend: String) {
        if (username == usernameOfFriend) {
            throw NoEntityFoundException("You cannot add yourself")
        }
        val user = getUserByUsername(username)
        val friend = getUserByUsername(usernameOfFriend)
        if (user.friends.contains(friend.toResponse())) {
            throw DuplicatedEntityException("User ${user.username} is already friend with ${friend.username}")
        }
        user.friends.add(friend.toResponse())
        updateUser(user, user.id)
        friend.friends.add(user.toResponse())
        updateUser(friend, friend.id)
    }

    fun removeFriend(username: String, usernameOfFriend: String) {
        val user = getUserByUsername(username)
        val friend = getUserByUsername(usernameOfFriend)
        if (user.friends.contains(friend.toResponse())) {
            user.friends.remove(friend.toResponse())
            updateUser(user, user.id)
            friend.friends.remove(user.toResponse())
            updateUser(friend, friend.id)
        } else {
            throw NoEntityFoundException("User ${user.username} is not friend with ${friend.username}")
        }

    }

    private fun <T> withUser(id: String, dataBaseCRUDCommand: (user: User) -> T): T {
        return userRepository.findById(id).map {
            dataBaseCRUDCommand(it)
        }.orElseThrow {
            NoEntityFoundException("User with id $id not found")
        }
    }

    fun getFriendsForUser(username: String): List<UserResponse> {
        val user = getUserByUsername(username)
        return user.friends
    }

    companion object {
        private val log: Logger = LoggerFactory.getLogger(this::class.java)
    }
}
