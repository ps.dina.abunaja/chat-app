package com.progressoft.chatapp.service

import com.progressoft.chatapp.model.ChatMessage
import com.progressoft.chatapp.repository.ChatMessageRepository
import com.progressoft.chatapp.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ChatMessageService {
    @Autowired
    private lateinit var messageRepository: ChatMessageRepository

    @Autowired
    private lateinit var userRepository: UserRepository


    fun saveMessages(message: ChatMessage) {
        if (userRepository.existsByUsername(message.receiverName!!)) {
            messageRepository.insert(message)
        }
    }

    fun getAllMessages(): List<ChatMessage> {
        return messageRepository.findAll()
    }

    companion object {
        private val log: Logger = LoggerFactory.getLogger(this::class.java)
    }
}