package com.progressoft.chatapp.handler

import com.progressoft.chatapp.controller.entity.ExceptionResponse
import com.progressoft.chatapp.exceptions.NoEntityFoundException
import com.progressoft.chatapp.exceptions.ServiceException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandler : AbstractExceptionHandler() {
    @ExceptionHandler(ServiceException::class)
    fun entityException(entityException: Throwable): ResponseEntity<ExceptionResponse> {
        return if (entityException is NoEntityFoundException) {
            prepareExceptionResponse(entityException.message.toString(), HttpStatus.NOT_FOUND)
        } else {
            prepareExceptionResponse(entityException.message.toString(), HttpStatus.BAD_REQUEST)
        }
    }
}