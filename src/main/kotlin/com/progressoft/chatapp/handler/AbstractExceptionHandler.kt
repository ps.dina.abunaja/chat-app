package com.progressoft.chatapp.handler

import com.progressoft.chatapp.controller.entity.ExceptionResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

abstract class AbstractExceptionHandler {
    protected fun prepareExceptionResponse(
        exceptionMessage: String,
        status: HttpStatus
    ): ResponseEntity<ExceptionResponse> {
        return ResponseEntity(
            ExceptionResponse(exceptionMessage, status),
            status
        )
    }
}