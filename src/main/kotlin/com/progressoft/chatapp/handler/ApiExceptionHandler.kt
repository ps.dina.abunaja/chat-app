package com.progressoft.chatapp.handler

import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.progressoft.chatapp.controller.entity.ExceptionResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ApiExceptionHandler : AbstractExceptionHandler() {
    @ExceptionHandler(
        MethodArgumentNotValidException::class,
        MissingKotlinParameterException::class,
        HttpMessageNotReadableException::class
    )
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun validationException(throwable: Throwable): ResponseEntity<ExceptionResponse> {
        return prepareExceptionResponse(
            "Some of the fields are missing please check them again: ${throwable.cause.toString()}",
            HttpStatus.BAD_REQUEST
        )
    }
}