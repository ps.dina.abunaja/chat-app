package com.progressoft.chatapp.repository

import com.progressoft.chatapp.model.User
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : MongoRepository<User, String> {
    fun existsByEmail(email: String): Boolean
    fun getUserByEmail(email: String): User
    fun getUserByUsername(username: String): User
    fun existsByUsername(username: String): Boolean
}