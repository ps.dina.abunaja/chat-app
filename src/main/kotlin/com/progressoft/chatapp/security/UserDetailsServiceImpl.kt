package com.progressoft.chatapp.security

import com.progressoft.chatapp.repository.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserDetailsServiceImpl : UserDetailsService {

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    private lateinit var userRepository: UserRepository

    override fun loadUserByUsername(username: String): UserDetails {
        try {
            val user = userRepository.getUserByUsername(username)
            val authorities = ArrayList<SimpleGrantedAuthority>()
            authorities.add(SimpleGrantedAuthority("ROLE_" + user.roles.name))
            log.info("load user with username ${user.username} that has authorities $authorities")
            println(user.username)
            return org.springframework.security.core.userdetails.User(
                user.username,
                passwordEncoder.encode(user.password),
                authorities
            )
        } catch (username: UsernameNotFoundException) {
            throw UsernameNotFoundException("User with $username not found")
        }
    }

    companion object {
        private val log: Logger = LoggerFactory.getLogger(this::class.java)
    }
}