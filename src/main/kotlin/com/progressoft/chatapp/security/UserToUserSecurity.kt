package com.progressoft.chatapp.security

import com.progressoft.chatapp.model.User

fun User.toSecurityDetails(): UserSecurityDetails {
    return UserSecurityDetails(
        this.email,
        this.password,
        this.roles.name,
    )
}