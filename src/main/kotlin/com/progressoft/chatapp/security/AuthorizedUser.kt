package com.progressoft.chatapp.security

import com.progressoft.chatapp.model.Role

class AuthorizedUser(
    val username: String,
    val role: Role
)
