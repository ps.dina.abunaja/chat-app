package com.progressoft.chatapp.security.jwt

import com.progressoft.chatapp.model.Role
import com.progressoft.chatapp.security.AuthorizedUser
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import javax.servlet.http.HttpServletRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component

@Component
class JwtVerificationProcessor {

    @Autowired
    private lateinit var jwtConfiguration: JwtConfiguration

    fun isValidAuthorizationHeader(header: String?): Boolean {
        return !header.isNullOrBlank() && header.startsWith(jwtConfiguration.tokenPrefix)
    }

    fun extractTokenFromHeader(header: String): String {
        return header.replace("Bearer ", "")
    }

    fun parsingToken(token: String): Claims {
        return Jwts.parser().setSigningKey(jwtConfiguration.secretKey).parseClaimsJws(token).body;
    }

    @Suppress("UNCHECKED_CAST")
    fun authenticateUser(parsedToken: Claims, request: HttpServletRequest) {
        val username = parsedToken.subject
        val authorities = parsedToken["authorities"] as List<Map<String, String>>
        val role = Role.valueOf(authorities.first()["authority"].toString().substring(5))
        val authorizationList = authorities.map {
            SimpleGrantedAuthority(it["authority"])
        }.toList()
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(
            AuthorizedUser(
                username, role
            ), null, authorizationList
        )
    }

}