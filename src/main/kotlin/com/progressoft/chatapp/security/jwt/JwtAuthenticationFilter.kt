package com.progressoft.chatapp.security.jwt

import com.fasterxml.jackson.databind.ObjectMapper
import com.progressoft.chatapp.exceptions.AccessAuthenticationException
import com.progressoft.chatapp.exceptions.HttpException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.io.IOException
import java.sql.Date
import java.time.LocalDate
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


class JwtAuthenticationFilter(
    authenticationManager: AuthenticationManager,
    private var jwtConfiguration: JwtConfiguration
) : UsernamePasswordAuthenticationFilter(authenticationManager) {

    override fun attemptAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse
    ): Authentication {
        try {
            val authenticationRequest: UsernameAndPasswordRequest = ObjectMapper()
                .readValue(request.inputStream, UsernameAndPasswordRequest::class.java)
            return authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                    authenticationRequest.username,
                    authenticationRequest.password
                )
            )
        } catch (exception: IOException) {
            throw HttpException(exception.message)
        } catch (failureAuthentication: AuthenticationException) {
            log.error("Authentication failed: this user does not have access")
            throw AccessAuthenticationException("Authentication failed: this user does not have access")
        }
    }

    override fun successfulAuthentication(
        request: HttpServletRequest,
        response: HttpServletResponse,
        chain: FilterChain,
        authResult: Authentication
    ) {
        val token = Jwts.builder()
            .setSubject(authResult.name)
            .claim("authorities", authResult.authorities)
            .setIssuedAt(java.util.Date())
            .setExpiration(Date.valueOf(LocalDate.now().plusDays(jwtConfiguration.tokenExpirationAfterDays.toLong())))
            .signWith(SignatureAlgorithm.HS512, jwtConfiguration.secretKey)
            .compact()
        setAuthorizationHeaders(response, token)
    }

    private fun setAuthorizationHeaders(response: HttpServletResponse, token: String) {
        response.setHeader("Access-Control-Expose-Headers", "token")
        response.addHeader("token", token)
    }

    companion object {
        private val log: Logger = LoggerFactory.getLogger(this.javaClass)
    }
}
