package com.progressoft.chatapp.security.jwt

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@ConfigurationProperties(prefix = "application.jwt")
@Configuration
class JwtConfiguration {
    var secretKey: String = "secret"
    var tokenExpirationAfterDays: String = (10 * 60 * 1000).toString()
    val tokenPrefix: String = "Bearer "
}

