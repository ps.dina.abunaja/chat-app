package com.progressoft.chatapp.security

import com.progressoft.chatapp.security.jwt.JwtAuthenticationFilter
import com.progressoft.chatapp.security.jwt.JwtConfiguration
import com.progressoft.chatapp.security.jwt.JwtVerificationFilter
import com.progressoft.chatapp.security.jwt.JwtVerificationProcessor
import com.progressoft.chatapp.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod.GET
import org.springframework.http.HttpMethod.POST
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SpringSecurityConfiguration : WebSecurityConfigurerAdapter() {

    @Autowired
    lateinit var userDetailsServiceImpl: UserDetailsServiceImpl

    @Autowired
    lateinit var jwtConfiguration: JwtConfiguration

    @Autowired
    lateinit var verificationProcessor: JwtVerificationProcessor
    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsServiceImpl)
    }

    override fun configure(http: HttpSecurity) {
        http
            .cors().and()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .addFilter(JwtAuthenticationFilter(authenticationManager(), jwtConfiguration))
            .addFilterAfter(
                JwtVerificationFilter(verificationProcessor),
                JwtAuthenticationFilter::class.java
            )
            .authorizeRequests()
            .antMatchers(POST, "/login")
            .permitAll().antMatchers(POST, "/api/v1/users").permitAll()
            .antMatchers("/chat/**").permitAll()
            .antMatchers(GET, "/api/v1/messages/").permitAll()
            .anyRequest()
            .authenticated()
    }
}