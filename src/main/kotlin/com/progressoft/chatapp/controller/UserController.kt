package com.progressoft.chatapp.controller

import com.progressoft.chatapp.model.User
import com.progressoft.chatapp.controller.entity.UserResponse
import com.progressoft.chatapp.controller.entity.toResponse
import com.progressoft.chatapp.security.jwt.UsernameAndPasswordRequest
import com.progressoft.chatapp.service.UserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.ACCEPTED
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
@CrossOrigin(origins = ["http://localhost:3000/"])
class UserController {
    @Autowired
    lateinit var userService: UserService

    @PostMapping("/login")
    fun signIn(@RequestBody user: UsernameAndPasswordRequest) {
        println(user)
    }

    @GetMapping("/api/v1/users")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    fun listUsers(): List<User> {
        log.info("display all users")
        return userService.listAllUsers()
    }

    @GetMapping("/api/v1/users/{email}")
    fun getUserByEmail(@PathVariable email: String): ResponseEntity<User> {
        return ResponseEntity(userService.getUserByEmail(email), ACCEPTED)
    }

    @PostMapping("/api/v1/users")
    fun registerUser(@RequestBody user: User): ResponseEntity<UserResponse> {
        return ResponseEntity(userService.addNewUser(user).toResponse(), CREATED)
    }

    @DeleteMapping("/api/v1/users/{email}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    fun deleteUser(@PathVariable email: String): ResponseEntity<HttpStatus> {
        userService.deleteUserByEmail(email)
        return ResponseEntity(ACCEPTED)
    }

    @PutMapping("/api/v1/users")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    fun updateUser(@RequestBody user: User): ResponseEntity<UserResponse> {
        return ResponseEntity(
            userService.updateUser(user, userService.getUserByEmail(user.email).id).toResponse(),
            ACCEPTED
        )
    }
    companion object {
        private val log: Logger = LoggerFactory.getLogger(this::class.java)
    }
}