package com.progressoft.chatapp.controller

import com.progressoft.chatapp.model.ChatMessage
import com.progressoft.chatapp.service.ChatMessageService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin(origins = ["http://localhost:3000/"])
// TODO you should use property placeholders for such urls to replace this value or have this configured through configuration bean
class ChatController {
    @Autowired
    private lateinit var simpMessagingTemplate: SimpMessagingTemplate

    @Autowired
    private lateinit var chatMessageService: ChatMessageService


    @MessageMapping("/private-message")
    fun sendMessage(@Payload message: ChatMessage): ChatMessage {
        log.info("handling send message: ${message.message} from ${message.senderName} to ${message.receiverName}")
        simpMessagingTemplate.convertAndSendToUser(message.receiverName!!, "/private", message)
        chatMessageService.saveMessages(message)
        return message
    }

    @GetMapping("api/v1/messages")
    fun getAllMessages(): List<ChatMessage> {
        return chatMessageService.getAllMessages()
    }
    companion object {
        private val log: Logger = LoggerFactory.getLogger(this::class.java)
    }
}