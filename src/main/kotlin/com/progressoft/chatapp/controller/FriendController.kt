package com.progressoft.chatapp.controller

import com.progressoft.chatapp.controller.entity.UserResponse
import com.progressoft.chatapp.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1/{username}")
@CrossOrigin(origins = ["http://localhost:3000/"])
class FriendController {
    @Autowired
    lateinit var userService: UserService

    @GetMapping
    fun listAllFriendsOfUser(@PathVariable username: String): ResponseEntity<List<UserResponse>> {
        return ResponseEntity(userService.getFriendsForUser(username), HttpStatus.ACCEPTED)
    }

    @PostMapping("{usernameOfFriend}")
    fun addFriendToUserByUsername(
        @PathVariable username: String,
        @PathVariable usernameOfFriend: String
    ): ResponseEntity<HttpStatus> {
        userService.addFriend(username, usernameOfFriend)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PutMapping("{usernameOfFriend}")
    fun removeFriend(
        @PathVariable username: String,
        @PathVariable usernameOfFriend: String
    ): ResponseEntity<HttpStatus> {
        userService.removeFriend(username, usernameOfFriend)
        return ResponseEntity(HttpStatus.ACCEPTED)
    }
}