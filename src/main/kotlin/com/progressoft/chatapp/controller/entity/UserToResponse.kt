package com.progressoft.chatapp.controller.entity

import com.progressoft.chatapp.model.User

fun User.toResponse(): UserResponse {
    return UserResponse(
        this.username,
        this.email,
        this.firstName,
        this.lastName,
        this.gender
    )
}
