package com.progressoft.chatapp.controller.entity

import com.progressoft.chatapp.model.Gender
data class UserResponse(
    val username: String,
    val email: String,
    val firstName: String,
    val lastName: String,
    val gender: Gender
)
