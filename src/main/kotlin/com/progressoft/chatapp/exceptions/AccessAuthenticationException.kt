package com.progressoft.chatapp.exceptions

open class AccessAuthenticationException : Exception {
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(message: String?) : super(message)
}
