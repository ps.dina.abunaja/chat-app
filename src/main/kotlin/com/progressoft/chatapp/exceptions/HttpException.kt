package com.progressoft.chatapp.exceptions

class HttpException : java.lang.RuntimeException {
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(message: String?) : super(message)
}