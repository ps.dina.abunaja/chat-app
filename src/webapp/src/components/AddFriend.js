import React, {Component} from "react";
import axios from "axios";
import {Button, Card, Form, Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAdd, faTrash} from "@fortawesome/free-solid-svg-icons";
import MyToasts from "./MyToasts";

const BASE_URL = "http://localhost:8080/api/v1/";

class AddFriend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            friends: [],
            show: false,
            message: "",
            type: "danger"
        }
    }

    componentDidMount() {
        // TODO the prefix of the url http://localhost:8080 should be configurable
        // TODO also, I think there should be an interceptor concept in those client libraries to inject this Authorization header instead of copy pasring the code
        //  or, you should have a separate JS file (class) as a service for communication and encapsulate the whole thing there
        //  this is a note for all similar code below and any other class
        axios.get(BASE_URL+"users", {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(response => response.data)
            .then((data) => {
                this.setState({
                    users: data
                });
            })
        let {username} = this.props.match.params
        axios.get(BASE_URL + username, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`
                }
            }
        )
            .then(response => {
                if (response.data != null)
                    this.setState({
                        friends: response.data
                    });
            })

    }

    refreshPage = () => {
        window.location.reload();
    }

    handleAddFriend = (usernameOfFriend, username) => {
        axios.post(BASE_URL + username + "/" + usernameOfFriend,
            {}, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`
                }
            })
            .then(response => {
                    if (response.data != null) {
                        this.refreshPage()

                    }
                }
            ).catch((err) => {
            this.setState({
                show: true,
                type: "danger",
                message: `${err}`
            })

        })
    }
    handleRemoveFriend = (username, usernameOfFriend) => {
        axios.put(`http://localhost:8080/api/v1/${username}/${usernameOfFriend}`, {}, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(response => {
                if (response.data != null) {
                    this.refreshPage()
                }
            }).catch((err) => {
            this.setState({show: true, type: "danger", message: `${username} not friend with ${usernameOfFriend}`})
            setTimeout(() => {
                this.setState({show: false})

            }, 3000)
        })
    }

    render() {
        let {username} = this.props.match.params
        return (
            <div>
                <div style={{"display": this.state.show ? "block" : "none"}}>
                    <MyToasts show={this.state.show} type={this.state.type}
                              message={this.state.message}/>
                </div>
                <Card className={"border-0 text-black form"}>
                    <Card.Header className={"text-center"}>Users</Card.Header>
                    <Card.Body>
                        <Table striped hover size={"sm"} width="200">
                            <thead>
                            <tr>
                                <td>Username</td>
                                <td>Actions</td>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.users.length === 0 ?
                                <tr align="center">
                                    <td colSpan="2">No Users Available.</td>
                                </tr> :
                                this.state.users.filter(user => user.username !== username).map(user => (
                                    <tr key={user.email}>
                                        <td>{user.username}</td>
                                        <td>
                                            {this.state.friends.some((item) =>
                                                item.username === user.username) ?
                                                <Button size={"sm"} variant={"outline-danger"}
                                                        onClick={this.handleRemoveFriend.bind(this, user.username, username)}>< FontAwesomeIcon
                                                    icon={faTrash}/>{" "}</Button> :
                                                <Button size={"sm"} variant={"outline-primary"}
                                                        onClick={this.handleAddFriend.bind(this, user.username, username)}>< FontAwesomeIcon
                                                    icon={faAdd}/>{" "}</Button>
                                            }
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </Table>
                        <Form.Label className={"mt-2 me-2 d-grid justify-content-center"}>
                            Continue to Chat!!
                            <span className="line">
                    <a href={`/chatRoom/${username}`}>Chat Room</a>
                    </span>
                        </Form.Label>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}

export default AddFriend
