import React, {Component} from 'react';
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import axios from "axios";
import MyToasts from "./MyToasts";

class SignUpUser extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialState;
        this.state.show = false
        this.state.show = false
        this.state.type = "danger"
    }

    initialState = {
        firstName: "",
        lastName: "",
        username: "",
        email: "",
        gender: "",
        password: ""
    }
    userChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
        })
    }

    submitUser = (event) => {
        event.preventDefault();
        const user = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            username: this.state.username,
            email: this.state.email,
            gender: this.state.gender,
            password: this.state.password
        }
        axios.post("http://localhost:8080/api/v1/users", user)
            .then(response => {
                if (response.data != null) {
                    this.setState(this.initialState);
                    this.setState({show: true, type: "success"})
                    setTimeout(() => {
                        this.setState({show: false})
                    }, 3000)
                }
            }).catch(() => {
            this.setState({show: true, type: "danger"})
            setTimeout(() => {
                this.setState({show: false})
            }, 3000)
        })
    }
    resetAll = () => {
        this.setState(this.initialState)
    }

    render() {
        const {username, firstName, lastName, email, gender, password} = this.state
        return (
            <div>
                <div style={{"display": this.state.show ? "block" : "none"}}>
                    <MyToasts show={this.state.show}
                              message={this.state.type === "success" ? "User Registered Successfully" : "user email is already exist or username is token"}
                              type={this.state.type}/>
                </div>
                <div className={"form-wrapper is-active"}>
                    <Card className={"form form-signup"}>
                        <h4 className={"text-center"}>Register New User</h4>
                        <hr/>
                        <Form
                            onReset={this.resetAll}
                            onSubmit={this.submitUser}
                        >
                            <Card.Body>
                                <Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>Firstname</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="firstName"
                                            value={firstName}
                                            onChange={this.userChange}
                                            className={"bg-white text-dark"}
                                            placeholder="firstname"
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col}>
                                        <Form.Label>Lastname</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="lastName"
                                            value={lastName}
                                            onChange={this.userChange}
                                            className={"bg-white text-dark"}
                                            placeholder="lastname"
                                        />
                                    </Form.Group>
                                </Row>
                                <Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="username"
                                            value={username}
                                            onChange={this.userChange}
                                            className={"bg-white text-dark"}
                                            placeholder="username"
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col}>
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control
                                            required
                                            type="email"
                                            name="email"
                                            value={email}
                                            onChange={this.userChange}
                                            className={"bg-white text-dark"}
                                            placeholder="email"
                                        />
                                        <Form.Text className="text-muted">
                                            We'll never share your email with anyone else.
                                        </Form.Text>
                                    </Form.Group>
                                </Row>
                                <Row>
                                    <Form.Group as={Col}>
                                        <Form.Label>Gender</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            name="gender"
                                            value={gender}
                                            onChange={this.userChange}
                                            className={"bg-white text-dark"}
                                            placeholder="gender"
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col}>
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            required
                                            type="password"
                                            name="password"
                                            value={password}
                                            onChange={this.userChange}
                                            className={"bg-white text-dark"}
                                            placeholder="password"
                                        />
                                    </Form.Group>
                                </Row>
                                <>
                                    <Button className="btn-signup mt-2 " size="sm" type="submit">Register</Button>{' '}
                                    <Button className="btn-clear mt-2" size="sm" type="reset">Clear All</Button>
                                </>
                            </Card.Body>
                            <Form.Label className={"mt-2 me-2 d-grid justify-content-end"}>
                                Have an Account?
                                <span className="line">
                                <a href={"/signIn"}> SignIn</a>
                            </span>
                            </Form.Label>
                        </Form>
                    </Card>
                </div>
            </div>
        );
    }
}

export default SignUpUser;