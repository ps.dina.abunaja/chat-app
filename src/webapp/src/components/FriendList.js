import React, {Component} from "react";
import {Card, Form} from "react-bootstrap";
import axios from "axios";
import ChatRoom from "./ChatRoom";
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";

export default class FriendList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            friends: [],
            errorMessage: ""
        };
    }

    componentDidMount() {
        let {username} = this.props.match.params
        axios.get("http://localhost:8080/api/v1/" + username,
            {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`
                }
            })
            .then(response => {
                if (response.data != null)
                    this.setState({
                        friends: response.data
                    });
            }).catch((err) => {
            this.setState({errorMessage: "no Authorization: you don't have access to chat"})
        })
    }

    render() {
        let {username} = this.props.match.params
        return (
            <div>
                {this.state.errorMessage !== "" ?
                    <Card className={"border-0 text-black form"}>
                        <Card.Body>
                            <div>{this.state.errorMessage}</div>
                        </Card.Body>
                    </Card>
                    :
                    <MDBContainer className={"bg-transparent text-black"}>
                        <MDBRow className="mx-md-n5">
                            <MDBCol>
                                <Form.Label className={"mt-2 me-2 d-grid"}>
                                <span>
                            <ChatRoom friends={this.state.friends}/>
                                </span>
                                </Form.Label>

                            </MDBCol>
                        </MDBRow>
                        <Form.Label className={"mt-2 me-2 d-grid justify-content-center"}>
                            Add New Friends?
                            <span className="line">
                    <a href={`/addFriend/${username}`}>Suggestion Users</a>
                    </span>
                        </Form.Label>
                    </MDBContainer>}
            </div>
        )
    }
}