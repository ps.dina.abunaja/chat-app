import React, {useEffect, useState} from 'react'
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import {useParams} from "react-router-dom";
import {v4 as uuid} from 'uuid';
import axios from "axios";
import {Button, Card} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPaperPlane} from "@fortawesome/free-solid-svg-icons";
import "./style.css"

let stompClient = null;

const ChatRoom = (props) => {
    let accessToken = localStorage.getItem("token")
    let {username} = useParams()
    const [privateChats, setPrivateChats] = useState(new Map());
    const [tab, setTab] = useState("CHATROOM");
    let friends = props.friends
    let [messageId] = uuid()
    let [messages, setMessages] = useState(new Map())

    const [userData, setUserData] = useState({
        username: username,
        receiverName: '',
        connected: false,
        message: '',
        messageId: ''
    });

    useEffect(() => {
        messageId = uuid()
    }, [userData]);

    const connect = () => {
        let Sock = new SockJS('http://localhost:8080/chat');
        stompClient = Stomp.over(Sock);
        stompClient.connect({}, onConnected, onError);
    }

    const onConnected = () => {
        console.log("connected to chat")
        handleGetMessages()
        initState()
        setUserData({...userData, "connected": true});
        stompClient.subscribe('/user/' + userData.username + '/private', onPrivateMessage);
        userJoin();
    }

    const onError = (err) => {
        console.log(err);
    }
    const initState = () => {
        friends.map(friend => privateChats.set(friend.username, []))
    }

    const onPrivateMessage = (payload) => {
        const payloadData = JSON.parse(payload.body);
        privateChats.get(payloadData.senderName).push(payloadData);
        setPrivateChats(new Map(privateChats));
    }

    const userJoin = () => {
        const chatMessage = {
            messageId: messageId,
            senderName: userData.username,
            status: "JOIN"
        };
        stompClient.send("/app/message", {}, JSON.stringify(chatMessage));
    }

    const sendPrivateValue = () => {
        if (stompClient) {
            const chatMessage = {
                senderName: userData.username,
                receiverName: tab,
                message: userData.message,
                status: "MESSAGE",
                messageId: messageId
            };
            if (userData.username !== tab) {
                privateChats.get(tab).push(chatMessage);
                setPrivateChats(new Map(privateChats));
            }
            stompClient.send("/app/private-message", {}, JSON.stringify(chatMessage));
            setUserData({...userData, "message": ""});
        }
    }
    const handleMessage = (event) => {
        const {value} = event.target;
        setUserData({...userData, "message": value});
    }
    const handleGetMessages = () => {
        axios.get("http://localhost:8080/api/v1/messages/", {
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            }
        ).then(response => {
            if (response.data != null) {
                messages.set(0, response.data)
            }
        })
    }

    return (
        <div className="container">
            <Card.Header
                className="text-center h5 mb-0 py-1 bg-gray px-4 py-2">Hello {userData.username}!!</Card.Header>
            {userData.connected ?
                <div className="chat-box">
                    <div className="member-list pt-1">
                        <ul>
                            <div className="px-4 py-2">
                                <p className="h5 mb-0 py-1">Your Friends</p>
                            </div>
                            {friends.length === 0 ?
                                <div>
                                    You Don't Have Any Friends Yet?
                                    <br/>
                                    Let's add some to start chat!!😁
                                </div>
                                :
                                [...friends].map((friend, index) => (
                                    <li onClick={() => {
                                        setTab(friend.username)
                                    }}
                                        className={`member  ${tab === friend.username && "active"}`}
                                        key={index}>{friend.username}</li>
                                ))
                            }
                        </ul>
                    </div>
                    {tab !== "CHATROOM" && <div className="chat-content">
                        <ul className="chat-messages">
                            {[...messages.get(0)].map((chat, index) => (
                                    (chat.receiverName === tab && chat.senderName === userData.username)
                                    || (chat.receiverName === userData.username && chat.senderName === tab) ?
                                        <li className={`message ${chat.senderName === userData.username && "self"}`}
                                            key={index}>
                                            {chat.senderName !== userData.username &&
                                                <div className="avatar">{chat.senderName}</div>}
                                            <div className="message-data">{chat.message}</div>
                                            {chat.senderName === userData.username &&
                                                <div className="avatar self">{chat.senderName}</div>}
                                        < /li> : null
                                )
                            )}
                            {[...privateChats.get(tab)].map((chat, index) => (
                                <li className={`message ${chat.senderName === userData.username && "self"}`}
                                    key={index}>
                                    {chat.senderName !== userData.username &&
                                        <div className="avatar">{chat.senderName}</div>}
                                    <div className="message-data">{chat.message}</div>
                                    {chat.senderName === userData.username &&
                                        <div className="avatar self">{chat.senderName}</div>}
                                </li>
                            ))}
                        </ul>

                        <div className="send-message">
                            <input type="text" className="input-message" placeholder="enter the message"
                                   value={userData.message} onChange={handleMessage}/>
                            <Button size={"sm"} variant={"outline-primary"} className={"mx-2 border-0"}
                                    onClick={sendPrivateValue}><FontAwesomeIcon
                                icon={faPaperPlane}/>Send
                            </Button>
                        </div>
                    </div>}
                </div>
                :
                connect()}
        </div>
    )
}

export default ChatRoom
