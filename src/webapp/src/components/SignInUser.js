import React, {useState} from 'react';
import {Form} from "react-bootstrap";
import axios from "axios";
import MyToasts from "./MyToasts";
import {faRightToBracket} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const SignInUser = (props) => {
    const [type, setType] = useState("danger")
    const [show, setShow] = useState(false)
    localStorage.setItem('token', "")
    const [userData, setUserData] = useState({
        username: "",
        password: ""
    })

    const initialState = {
        username: "",
        password: ""
    }
    const userChange = (event) => {
        const {name, value} = event.target
        setUserData({...userData, [name]: value})
    }
    const submitUser = (event) => {
        event.preventDefault();
        axios.post("http://localhost:8080/login", userData)
            .then(response => {
                localStorage.setItem('user', JSON.stringify(userData))
                localStorage.setItem('token', response.headers.token)
                return props.history.push({
                    pathname: `/chatRoom/${userData.username}`
                });
            }).catch((err) => {
            setType("danger")
            setShow(true)
            setUserData(initialState)
            setTimeout(() => {
                setShow(false)
            }, 3000)
        })
    }

    return (
        <div>
            <div style={{"display": show ? "block" : "none"}}>
                <MyToasts show={show} type={type}
                          message={type === "success" ? "Login Successfully" : "Password or email is incorrect"}/>
            </div>
            <div className="form-wrapper is-active align-content-center">
                <div className="form form-login">
                    <h4 className={"text-center"}>SignIn</h4>
                    <hr/>
                    <fieldset>
                        <legend>Please enter your email and password for login.</legend>
                        <div className="input-block">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                required
                                type="username"
                                name="username"
                                value={userData.username}
                                onChange={userChange}
                                placeholder="username"
                            />
                        </div>
                        <div className="input-block">
                            <Form.Label htmlFor="login_password">Password</Form.Label>
                            <Form.Control
                                required
                                type="password"
                                name="password"
                                value={userData.password}
                                onChange={userChange}
                                placeholder="password"
                            />
                        </div>
                    </fieldset>
                    <button className="btn-login" type="submit" onClick={submitUser}>
                        <FontAwesomeIcon icon={faRightToBracket} className={"mx-1"}/>
                        Log In
                    </button>
                    <Form.Label className={"mt-2 d-flex justify-content-end"}>
                        Need an Account?
                        <span className="line">
                               <a href={"/signUp"}>SignUp</a>
                            </span>
                    </Form.Label>
                </div>
            </div>
        </div>
    )

}

export default SignInUser;