import React, {useEffect, useState} from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSignInAlt, faSignOutAlt, faUserPlus} from "@fortawesome/free-solid-svg-icons";

const NavigationBar = () => {
    const [user, setUser] = useState({});
    useEffect(() => {
        setInterval(() => {
            const userString = localStorage.getItem("user");
            const user = JSON.parse(userString);
            setUser(user);
        }, [])
    }, [user]);

    const logout = () => {
        localStorage.removeItem("token")
        return localStorage.removeItem("user")
    }
    return (
        <div>
            <Navbar bg="transparent" variant={"light"}>
                <Nav className="navbar-brand ">
                    <img
                        alt=""
                        src="https://upload.wikimedia.org/wikipedia/commons/8/85/Circle-icons-chat.svg"
                        width="30"
                        height="30"
                        className="d-inline-block align-top ms-2"
                    />{" "}Chat-App
                </Nav>

                {user ?
                    <Navbar.Collapse className="justify-content-end">
                        <Link to={"/signIn"} className="nav-link" onClick={logout}> <FontAwesomeIcon
                            icon={faSignOutAlt}/> SignOut</Link>
                    </Navbar.Collapse>
                    :
                    <Navbar.Collapse className="justify-content-end">
                        <Link to={"/signUp"} className="nav-link"> <FontAwesomeIcon
                            icon={faUserPlus}/> SignUp</Link>
                        <Link to={"/signIn"} className="nav-link"> <FontAwesomeIcon
                            icon={faSignInAlt}/> SignIn</Link>
                    </Navbar.Collapse>
                }
            </Navbar>
        </div>
    );

}

export default NavigationBar;