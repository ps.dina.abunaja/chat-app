import './App.css';
import {Col, Container, Row} from "react-bootstrap";
import SignUpUser from "./components/SignUpUser";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import SignInUser from "./components/SignInUser";
import FriendList from "./components/FriendList";
import NavigationBar from "./components/NavigationBar";
import AddFriend from "./components/AddFriend";

function App() {
    return (
        <BrowserRouter>
            <NavigationBar/>
            <Container>
                <Row>
                    <Col lg={12} className={"text-muted mx-auto"}>
                        <Switch>
                            <Route path="/" exact component={SignInUser}/>
                            <Route path="/signUp" exact component={SignUpUser}/>
                            <Route path="/signIn" exact component={SignInUser}/>
                            <Route path="/chatRoom/:username" exact component={FriendList}/>
                            <Route path="/addFriend/:username" exact component={AddFriend}/>
                        </Switch>
                    </Col>
                </Row>
            </Container>
        </BrowserRouter>
    );
}

export default App;
