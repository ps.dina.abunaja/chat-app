package com.progressoft.chatapp.service

import com.progressoft.chatapp.exceptions.DuplicatedEntityException
import com.progressoft.chatapp.exceptions.NoEntityFoundException
import com.progressoft.chatapp.model.Gender
import com.progressoft.chatapp.model.Role
import com.progressoft.chatapp.model.User
import com.progressoft.chatapp.repository.UserRepository
import java.util.Optional
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.not
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ActiveProfiles


@SpringBootTest
@ActiveProfiles("test")
internal class UserServiceTest {
    @Autowired
    private lateinit var userService: UserService

    @MockBean
    lateinit var userRepository: UserRepository
    private val email = "test@telelogx.com"
    private val user = User("testUser", "test", "one", Gender.FEMALE, email, "12345678$", roles = Role.USER)
    private val friendUser =
        User("friendUser", "new", "user", Gender.FEMALE, "friendUser@gmail.com", "12345678$", roles= Role.USER)

    @BeforeEach
    internal fun setUp() {
        userRepository.deleteAll()
    }

    @Test
    internal fun `user should be stored`() {
        `when`(userRepository.insert(user)).thenReturn(user)
        userService.addNewUser(user)
        `when`(userRepository.findAll()).thenReturn(listOf(user))
        assertThat(userService.listAllUsers())
            .contains(user)
    }

    @Test
    internal fun `user should be deleted if exist`() {
        `when`(userRepository.insert(user)).thenReturn(user)
        userService.addNewUser(user)
        `when`(userRepository.existsByEmail(email)).thenReturn(true)
        userService.deleteUserByEmail(email)
        assertThat(userService.listAllUsers(), not(userService.listAllUsers().contains(user)))
    }

    @Test
    internal fun `different users with same email should not be_registered`() {
        val newUser = User("newUser", "new", "user", Gender.FEMALE, "newUser@gmail.com", "12345678$", roles = Role.USER)
        `when`(userRepository.insert(newUser)).thenReturn(newUser)
        `when`(userRepository.findAll()).thenReturn(listOf(newUser))
        userService.addNewUser(newUser)
        `when`(userRepository.existsByEmail(newUser.email)).thenReturn(true)
        assertThatThrownBy {
            userService.addNewUser(newUser)
        }.isInstanceOf(DuplicatedEntityException::class.java)
            .hasMessage(
                "user with email ${newUser.email} is already exist or username ${newUser.username} is token"
            )
    }

    @Test
    internal fun `delete unexciting user should throw an exception`() {
        val email = "dd@gmail.com"
        assertThatThrownBy { userService.deleteUserByEmail(email) }
            .isInstanceOf(NoEntityFoundException::class.java)
            .hasMessage("User with email $email not exist")
    }

    @Test
    internal fun `should add friend to user if they are not friend`() {
        addFriendToUser(friendUser)

        assertThatThrownBy { userService.addFriend(user.username, friendUser.username) }.isInstanceOf(
            DuplicatedEntityException::class.java
        ).hasMessage("User ${user.username} is already friend with ${friendUser.username}")
    }

    @Test
    internal fun `should user remove friend if they are friends`() {
        addFriendToUser(friendUser)

        userService.removeFriend(user.username, friendUser.username)
        assertThatThrownBy { userService.removeFriend(user.username, friendUser.username) }.isInstanceOf(
            NoEntityFoundException::class.java
        ).hasMessage("User ${user.username} is not friend with ${friendUser.username}")
    }

    @Test
    internal fun `user should not can add himself`() {
        assertThatThrownBy { addFriendToUser(user) }.isInstanceOf(
            NoEntityFoundException::class.java
        ).hasMessage("You cannot add yourself")
    }

    private fun addFriendToUser(friendUser: User) {
        `when`(userRepository.insert(user)).thenReturn(user)
        `when`(userRepository.insert(friendUser)).thenReturn(friendUser)
        userService.addNewUser(user)
        userService.addNewUser(friendUser)
        `when`(userRepository.existsByUsername(user.username)).thenReturn(true)
        `when`(userRepository.existsByUsername(friendUser.username)).thenReturn(true)
        `when`(userService.getUserByUsername(user.username)).thenReturn(user)
        `when`(userService.getUserByUsername(friendUser.username)).thenReturn(friendUser)
        user.id = "1"
        friendUser.id = "2"
        `when`(userRepository.findById(user.id)).thenReturn(Optional.of(user))
        `when`(userRepository.findById(friendUser.id)).thenReturn(Optional.of(friendUser))

        `when`(userRepository.save(user)).thenReturn(user)
        `when`(userRepository.save(friendUser)).thenReturn(friendUser)

        userService.addFriend(user.username, friendUser.username)
    }
}